#!/bin/bash
#
# This was an assigment in one "functional programming night" meetup. We were
# supposed to implement a program that would receive integers (breakline
# separated) in one TCP port and return them plus one in another TCP
# port. Needless to say, bash was most elegant compared to all those functional
# things :P (joking, just in case ...)

PORT_IN=$1
IP_OUT=$2
PORT_OUT=$3

while read INPUT
do
    echo $(($INPUT + 1)) | netcat $IP_OUT $PORT_OUT
done < <(netcat -l $PORT_IN)
